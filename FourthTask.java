import java.util.Random;

public class FourthTask {
    public static void main(String[] args) {
        Random rand = new Random();
        int[] data = new int[8];
        for(int i = 0; i < data.length; i++) {
            data[i] = rand.nextInt(10) + 1;
        }

        for(int i = 0; i < data.length; i++){
            System.out.print(data[i] + " ");
        }
        System.out.println();

        boolean isGrowing = true;

        for(int i = 1; i < data.length; i++){
            if(data[i] >= data[i + 1]){
                isGrowing = false;
                break;
            }
        }

        if(isGrowing){
            System.out.println("The sequence increases");
        }
        else{
            System.out.println("The sequence does not increases");
        }

        for(int i = 0; i < data.length; i++){
            if (i % 2 == 1) {
                data[i] = 0;
            }
        }

        for(int i = 0; i < data.length; i++){
            System.out.print(data[i] + " ");
        }
    }
}
